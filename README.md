# README #

### What is this repository for? ###

This trial-root scheme is an alternative approach to the existing dual-root schemes that CoreOS and SnappyOS are using. The dual-root partition scheme is a great improvement over tools like yum or apt-get.  However system update is only available for the distribution specific release update channels, and updates are for kernel+initrd, bootloaders won't get updated same time. The ROOT partition you are running may not get automatic update from these distribution channels.

In this trial-root scheme the system tries to answer to the above issues and uses more root partitions on the disk/image. System is booted with a bootloader from BOOT-B into ROOT-B partition. ROOT-B is mounted as the real root filesystem. root-update begins talking to a configured update service. If there is an update available it is downloaded and installed into BOOT-A/ROOT-A. Update is finished off by switching partition labels.

BOOT-B/ROOT-B to BOOT-C/ROOT-C
 
BOOT-A/ROOT-A to BOOT-B/ROOT-B

Previous partitions are always safe on BOOT-C/ROOT-C

Version 0.1

### How do I get set up? ###

The basic idea is to create manually a working update & rollback(*) scheme for any Linux client/server environment, and mostly depending on your distribution/deployment see if it works automating the building of your environment. The creation of Linux client/server environment is entirely dependent on the choice of the target distribution. The system commands are standard commands that all GNU/Linux distributions usually have, i.e. blkid, mount, umount, md5sum, dd, e2label, parted.

The following will describe you the packaging of the required tools. Since the various distro's package things differently, the listed special dependencies might not be compatible with or named different in your distro. 

(*) For more information about rollback in Debian/Ubuntu based distribution see below under topic root-rollbacks or https://bitbucket.org/vsuojanen/root-rollbacks

**Filesystem depencies**

Supported filesystems are ext2-4

**Creating mount directories**

In your boot device root hierarchy create /images directory. This /images is just a mountpoint for the root image device files (*.img)
/host/images (root device ROOT-A) when system is running updates in the real client root filesystem

Now mount loopback device image or chroot into the rootdir (in your client root environment) and create there the mount point /host for the boot device. This /host is mountpoint for boot device BOOT-A when running in client root environment. When you run root-updates to your system they are installed into this /host (BOOT-A) and /host/images (ROOT-A)
 

**Tools & Software**

/usr/bin/update and /usr/sbin/update_check are the main scripts. The root-fs and root-rw function files are sourced from update_check and copied into /usr/share/root-updates directory. All system commands in the scripts/functions should be accessible manually, but this will depend on your Linux distribution.

mkdir /usr/share/root-updates

All these files should be copied executable.


cp root-fs /usr/share/root-updates/

cp root-rw /usr/share/root-updates/

cp client/update_check /usr/sbin/

cp update /usr/bin/


**Quick introduction:** 

The minimum update process works like following:

1. Download MD5/SHA256 hash file and update program
2. Run update_check, usage: '/usr/sbin/update_check 'CMD' DIR'
3. If there is update needed to one of the root filesystems - update_check mounts BOOT-A and ROOT-A partitions and runs your image/file update program
4. You can install (cp, rsync, dd, syslinux/extlinux) any file to one of the mounted BOOT-A/ROOT-A root filesystems
5. Switch partition labels and set the new boot device on
6. Reboot


**update**

Depencies:
`uuidgen`
`ssh`

usage: update 'UPDATE_SERVER'

You can run this command/program from crontab or manually or by whatever job or queue manager you use in your system. 

This program connects client to the update server (change or re-write the whole program to fit your needs e.x. ftp, http, ssh, ssl, use private keys, certs). This example tries to execute simple remote server command ./update-client that should output.  
the local command for running update
`ssh $user@$UPDATE_SERVER "./update-client"`

This generates local  update script. See more about this example below

**update-client (server example)**

Depencies:
`MD5SUMS`
Root update was designed originally so that update-client can just output the update command and output will be re-directed securely through the ssh connection to the client and then run locally. If you write your update-client script or binary, store  it inserver so that users can run it over ssh remotely as ./update-client. 

The script update-client (or binary) should print the real executable client command and update processes this standard output (STDOUT) to a file named COMMAND. The COMMAND will then run locally on the client from it's temporary download directory under /var/tmp. 

./COMMAND

The actions you need to write in COMMAND should minimum download MD5/SHA256 hashes and a program that installs you update files into the client system. Next your program should execute on client system the program /usr/sbin/update_check that will check if updates are really needed on local client and if there is update needed -  execute program that will install them  (see below example usage of update_check and client update install program)

**update_check**

Depencies:
`root-fs`
`root-rw`
`MD5SUMS`

System commands:
`blkid`
`findmnt`
`mount`
`umount`
`fstype`
`dd`
`e2label`
`md5sum`
`parted`

The update_check processing checks the files listed in  MD5SUMS and compares their checksums on boot device to the most recent MD5SUMS that client automatically downloaded from the  update server. The MD5 hashes for image files located in special ROOT-B partition need to be separated from the other files by marking up these special device files in MD5SUMS with a directory name "images". This ROOT-B partition is used for device files that client system mounts as root filesystem. When update_check resolves that update is needed it will trigger downloaded update program and run it,  two runs if both ROOT device and BOOT device need update. 

`/usr/sbin/update_check 'CMD' 'DIR'`

CMD will install your update files into the update device (once for each device ROOT or BOOT).


`CMD '[ROOT|BOOT]' "/root/utils"`

CMD should read and understand minimum two parameters to install your updates using the right device:

1) update device type 'ROOT' 


2) update device type 'BOOT'

After succesfull install command (CMD exit 0) update_check will automatically switch partition labels and set the new partition on in the partition table.

BOOT-B/ROOT-B to BOOT-C/ROOT-C.
BOOT-A/ROOT-A to BOOT-B/ROOT-B.

Previous partitions are always safe on BOOT-C/ROOT-C.

update_check will finish off root updates by setting the new boot partition on. 
Your new system is up on the client environment after reboot.



**root-rollbacks**

Optional, use only with Debian/Ubuntu specific environment

https://bitbucket.org/vsuojanen/root-rollbacks

*Depencies*

* initramfs-tools

`cp client/$DISTRO/* /usr`

With that done, you should have the following executable files in your target distribution /share directory.

`initramfs-tools/scripts/init-bottom/parted`

`initramfs-tools/scripts/init-premount/parted`

`initramfs-tools/scripts/root-fs`

`initramfs-tools/scripts/root-rw`

`initramfs-tools/hooks/disktools`

Regenerate the initramfs:

Make sure your target environment uses the new initramfs to boot:


**How to run tests**


**Deployment instructions**

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Project owner Valtteri Suojanen
* This project is free software; it is licensed under the terms of the GNU General Public License. Some parts of the source code must retain also the original BSD-derived license and copyright notice
